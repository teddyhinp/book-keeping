import { modeOption, setCurrentMode } from '@/redux/loginOrRegister'
import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

export function ToggleBtn() {
  const currentMode = useSelector((state) => state.loginOrRegister.currentMode)
  const dispatch = useDispatch()

  const handleButtonOnClick = (mode) => {
    dispatch(setCurrentMode(mode))
  }

  const buttonClass = (identifier) =>
    currentMode == identifier
      ? 'border-b-4 border-primary border-solid w-[100px] p-[5px]'
      : 'w-[100px] p-[5px]'

  return (
    <div className="flex gap-[10px] justify-center items-center">
      <button
        className={buttonClass(modeOption.LOGIN)}
        onClick={() => handleButtonOnClick(modeOption.LOGIN)}
      >
        登入
      </button>
      <button
        className={buttonClass(modeOption.REGISTER)}
        onClick={() => handleButtonOnClick(modeOption.REGISTER)}
      >
        註冊
      </button>
    </div>
  )
}
