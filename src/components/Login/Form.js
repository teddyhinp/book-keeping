import { modeOption, setIsLogin } from '@/redux/loginOrRegister'
import { Button, TextField } from '@mui/material'
import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { useDispatch, useSelector } from 'react-redux'
import auth from '@/api/auth'
import { TOKEN_NAME } from '@/constants/token_name'
import { useRouter } from 'next/router'
import { LOGIN_STATUS_ENUM, SIGN_UP_STATUS_ENUM } from '@/constants/status'

export function Form() {
  const router = useRouter()

  const currentMode = useSelector((state) => state.loginOrRegister.currentMode)
  const [loginStatus, setLoginStatus] = useState(LOGIN_STATUS_ENUM.NEUTRAL)
  const [signUpStatus, setSignUpStatus] = useState(SIGN_UP_STATUS_ENUM.NEUTRAL)
  const dispatch = useDispatch()

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  })

  const rowClass = 'flex items-center gap-[20px]'

  const onSubmit = async (data) => {
    try {
      if (currentMode == modeOption.REGISTER) {
        const result = await auth.signup(data)
        if (result.status == 200) {
          setSignUpStatus(SIGN_UP_STATUS_ENUM.SUCCESS)
        } else if (result.status == 403) {
          setSignUpStatus(SIGN_UP_STATUS_ENUM.FAIL)
        }
      } else {
        const result = await auth.login(data)
        const token = result.data.access_token
        if (token) {
          dispatch(setIsLogin(true))
          setLoginStatus(LOGIN_STATUS_ENUM.SUCCESS)
          localStorage.setItem(TOKEN_NAME, token)
          router.push('/wallet')
        } else {
          setLoginStatus(LOGIN_STATUS_ENUM.FAIL)
        }
      }
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <div
      className="flex flex-col items-center mt-[30px]"
      sx={{ '& .MuiInputBase-root': { height: '40px' } }}
    >
      <form className="flex flex-col gap-[10px] w-full px-5" onSubmit={handleSubmit(onSubmit)}>
        <div className={rowClass}>
          <TextField
            {...register('name')}
            placeholder="User Name"
            fullWidth
            error={!!errors.name}
            helperText={errors.name?.message}
          />
        </div>
        <div className={rowClass}>
          <TextField
            {...register('password')}
            type="password"
            placeholder="Password"
            fullWidth
            error={!!errors.password}
            helperText={errors.password?.message}
          />
        </div>
        {currentMode == modeOption.REGISTER && (
          <div className={rowClass}>
            <TextField
              {...register('confirmPassword')}
              type="password"
              placeholder="Confirm Password"
              fullWidth
              error={!!errors.confirmPassword}
              helperText={errors.confirmPassword?.message}
            />
          </div>
        )}
        {loginStatus == LOGIN_STATUS_ENUM.FAIL && currentMode == modeOption.LOGIN && (
          <div className="text-red-600 text-xs">Username or password is incorrect</div>
        )}
        {signUpStatus == SIGN_UP_STATUS_ENUM.SUCCESS && currentMode == modeOption.REGISTER && (
          <>
            <div className="text-green-400 text-xs"> Account created!</div>
            <div className="text-green-400 text-xs">Please log in to access your account.</div>
          </>
        )}
        <div className="flex justify-center gap-6 mt-[10px]">
          <Button variant="contained" type="button">
            忘記密碼
          </Button>
          <Button variant="contained" type="submit">
            登入
          </Button>
        </div>
      </form>
    </div>
  )
}

const schema = yup.object().shape({
  name: yup.string().required('name is required'),
  password: yup.string().required('Password is required'),
  confirmPassword: yup.string().oneOf([yup.ref('password'), null], 'Passwords must match'),
})
