import React from 'react'
import { ToggleBtn } from './ToggleBtn'
import { Form } from './Form'

export function Login() {
  return (
    <div className="flex justify-center items-start h-full md:items-center">
      <div className="border p-4 w-[300px] rounded-xl shadow-md">
        <ToggleBtn />
        <Form />
      </div>
    </div>
  )
}
