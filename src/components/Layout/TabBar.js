import { layoutTypographyEnum } from '@/constants'
import { links } from '@/constants/links'
import { Typography } from '@mui/material'
import Link from 'next/link'
import React from 'react'
import { useTranslation } from 'react-i18next'
export function TabBar() {
  return (
    <div className="flex w-full justify-between bg-primary p-[10px] fixed bottom-0 h-[80px] md:hidden">
      {links.map((el, i) => {
        return <TabItem item={el} key={i} />
      })}
    </div>
  )
}

function TabItem({ item }) {
  const { title, icon, href } = item
  const { t } = useTranslation()
  const displayIconOnly = title == 'add record'
  return (
    <div className="flex text-center m-auto no-underline md:hidden" href={href}>
      <Link href={href}>
        <div>{icon}</div>

        <Typography
          variant={layoutTypographyEnum.tabBarTitle}
          className={displayIconOnly ? 'hidden' : ''}
        >
          {t(title)}
        </Typography>
      </Link>
    </div>
  )
}
