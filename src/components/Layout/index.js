import React, { useEffect, useState } from 'react'
import { Header } from './Header'
import { Footer } from './Footer'
import { TabBar } from './TabBar'
import { TOKEN_NAME } from '@/constants/token_name'
import { useRouter } from 'next/router'
import { useTranslation } from 'react-i18next'
import { useDispatch, useSelector } from 'react-redux'
import { setIsLogin } from '@/redux/loginOrRegister'

export function Layout({ children }) {
  const isLogin = useSelector((state) => state.loginOrRegister.isLogin)
  const dispatch = useDispatch()
  useEffect(() => {
    const token = localStorage.getItem(TOKEN_NAME)
    if (token) {
      dispatch(setIsLogin(true))
    }
  }, [isLogin])
  const router = useRouter()
  const { t, i18n } = useTranslation()
  function changeLanguage(lang) {
    // Check if the language code is valid
    if (['en', 'tc'].indexOf(lang) !== -1) {
      console.log(i18n)
      // i18n.changeLanguage('tc')
      console.log(t('settng'))
    } else {
      console.warn(`Invalid language code: ${lang}`)
    }
  }
  useEffect(() => {
    if (router.query.lang) {
      changeLanguage(router.query.lang)
    }
  }, [router.query.lang])

  return (
    <>
      {isLogin && <Header />}
      <div className="max-w-[1600px] mx-auto h-full pt-[80px] sm:pt-[139px]">{children}</div>
      {isLogin && <TabBar />}
      <Footer />
    </>
  )
}
