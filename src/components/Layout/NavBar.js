import { layoutTypographyEnum } from '@/constants'
import { links } from '@/constants/links'
import { Box, Typography } from '@mui/material'
import Link from 'next/link'
import { useTranslation } from 'react-i18next'

export function NavBar() {
  return (
    <div className="hidden md:flex w-full justify-center items-center gap-[40px] bg-primary p-[10px]">
      {links.map((el, i) => {
        return <NavItem item={el} key={i} />
      })}
    </div>
  )
}

function NavItem({ item }) {
  const { title, icon, href } = item
  const { t } = useTranslation()
  const displayIconOnly = title == 'add record'
  return (
    <Link href={href}>
      <Box
        sx={{ '& svg': { color: 'white', width: '25px' } }}
        className={displayIconOnly ? '' : 'hidden'}
      >
        {icon}
      </Box>

      <Typography
        variant={layoutTypographyEnum.navBarTitle}
        className={displayIconOnly ? 'hidden' : ''}
      >
        {t(title)}
      </Typography>
    </Link>
  )
}
