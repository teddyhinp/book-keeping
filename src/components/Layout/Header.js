import { div, Typography } from '@mui/material'
import React from 'react'
import { NavBar } from './NavBar'
import { format } from 'date-fns'
import AccountCircleIcon from '@mui/icons-material/AccountCircle'
import AccountBalanceWalletIcon from '@mui/icons-material/AccountBalanceWallet'
import LogoutIcon from '@mui/icons-material/Logout'
import { useRouter } from 'next/router'
import { layoutTypographyEnum } from '@/constants'
import { TOKEN_NAME } from '@/constants/token_name'
import { setIsLogin } from '@/redux/loginOrRegister'
import { useDispatch } from 'react-redux'
export function Header() {
  const router = useRouter()
  const date = format(new Date(), 'yyyy年MM月dd日')
  const dispatch = useDispatch()

  const handleUserIconOnClick = () => {
    router.push('/setting')
  }
  const handleAppIconOnClick = () => {
    router.push('/wallet')
  }

  const handleLogout = () => {
    localStorage.removeItem(TOKEN_NAME)
    dispatch(setIsLogin(false))
    router.push('/')
  }
  return (
    <div className="fixed w-full">
      <div className="flex justify-between items-center p-5">
        <AccountBalanceWalletIcon onClick={handleAppIconOnClick} fontSize="large" />
        <Typography variant={layoutTypographyEnum.date}>{date}</Typography>
        <div className="flex gap-[10px]">
          <AccountCircleIcon onClick={handleUserIconOnClick} fontSize="large" />
          <LogoutIcon onClick={handleLogout} fontSize="large" />
        </div>
      </div>
      <NavBar />
    </div>
  )
}
