import { TOKEN_NAME } from '@/constants/token_name'
import { setIsLogin } from '@/redux/loginOrRegister'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

const withAuth = (WrappedComponent) => {
  const Auth = (props) => {
    const router = useRouter()
    const isLogin = useSelector((state) => state.loginOrRegister.isLogin)
    const dispatch = useDispatch()

    useEffect(() => {
      const token = localStorage.getItem(TOKEN_NAME)
      if (!token) {
        router.push('/')
        dispatch(setIsLogin(false))
      }
    }, [isLogin])

    return <WrappedComponent {...props} />
  }

  return Auth
}

export default withAuth
