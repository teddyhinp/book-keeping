import wallet from '@/api/wallet'
import React, { useEffect, useState } from 'react'
import ModeEditIcon from '@mui/icons-material/ModeEdit'
import { Typography } from '@mui/material'
import ClearOutlinedIcon from '@mui/icons-material/ClearOutlined'
import { useTranslation } from 'react-i18next'
export function WalletList() {
  const { t } = useTranslation()
  const [wallets, setWallets] = useState([])
  useEffect(() => {
    const fetchWallets = async () => {
      try {
        const { data } = await wallet.getMe()

        setWallets([...data.content, ...data.content, ...data.content])
      } catch (error) {
        console.log(error.message)
      }
    }
    fetchWallets()
  }, [])
  return (
    <div className="mx-auto flex flex-col items-center ">
      <Typography variant="h5">{t('Wallet List')}</Typography>
      <div className="flex flex-wrap  gap-[10px] mt-[10px] justify-center">
        {wallets.map((el, i) => (
          <WalletItem key={i} detail={el} />
        ))}
      </div>
    </div>
  )
}

function WalletItem({ detail }) {
  return (
    <div className="border w-[200px]  p-[15px] relative">
      <div>{detail.name}</div>
      <div>{detail.amount}</div>
      <ModeEditIcon className="absolute bottom-[5px] right-[5px] w-[20px]" />
      <ClearOutlinedIcon className="absolute top-[5px] right-[5px] w-[15px]" />
    </div>
  )
}
