import { walletTypographyEnum } from '@/constants/typography/wallet'
import { setOpen } from '@/redux/wallet'
import { Dialog, Typography } from '@mui/material'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { AddWalletForm } from './AddWalletForm'

export function AddWalletDialog() {
  const open = useSelector((state) => state.wallet.open)
  const dispatch = useDispatch()
  const handleClose = () => {
    dispatch(setOpen(false))
  }
  return (
    <Dialog onClose={handleClose} open={open}>
      <div className="w-[700px] p-[20px]">
        <Typography variant={walletTypographyEnum.DialogTitle}>Add Wallet</Typography>
        <AddWalletForm />
      </div>
    </Dialog>
  )
}
