import React from 'react'
import AddCircleIcon from '@mui/icons-material/AddCircle'
import { useDispatch, useSelector } from 'react-redux'
import { setOpen } from '@/redux/wallet'
export function AddWalletBtn() {
  const dispatch = useDispatch()
  const handleAddBtnOnClick = () => {
    dispatch(setOpen(true))
  }
  return (
    <div className="flex items-center justify-center">
      <AddCircleIcon fontSize="large" onClick={handleAddBtnOnClick} />
    </div>
  )
}
