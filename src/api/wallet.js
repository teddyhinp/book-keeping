import api from './index'

const subUrl = '/wallet'

const getMe = () => {
  return api.get(`${subUrl}/me`)
}

// const signup = (body) => {
//   return api.post(`${subUrl}/signup`, body)
// }

const wallet = {
  getMe,
}

export default wallet
