import { TOKEN_NAME } from '@/constants/token_name'
import axios from 'axios'

const baseURL = process.env.NEXT_PUBLIC_API_BASE_URL

const api = axios.create({
  baseURL,
  validateStatus: (status) => {
    return status >= 200 && status <= 502
  },
})

// Add a request interceptor
api.interceptors.request.use(
  (config) => {
    // Inject token to header
    const token = localStorage.getItem(TOKEN_NAME)
    if (token && config.headers) {
      config.headers['Authorization'] = `Bearer ${token}`
      console.debug(`Headers: ${config.headers['Authorization']}`)
    }
    return config
  },
  (error) => {
    // Do something with request error
    return Promise.reject(error)
  }
)

// Add a response interceptor
api.interceptors.response.use(
  (response) => {
    if (response.data.status === 401) {
      // remove token
      localStorage.removeItem(TOKEN_NAME)
      // redirect to login page
      window.location.href = '/login'
    }
    return response
  },
  (error) => {
    return Promise.reject(error)
  }
)

export default api

export const axiosErrorHandler = (res, error) => {
  if (error.isAxiosError) {
    console.log(error.message)
    let statusCode = error.response?.status || 500
    let rawResponse = error.response?.data || { error: true }
    return res.status(statusCode).json(rawResponse)
  } else {
    return res.status(500).json(error.message)
  }
}
