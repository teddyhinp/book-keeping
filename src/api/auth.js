import api from './index'

const subUrl = '/auth'

const login = (body) => {
  return api.post(`${subUrl}/login`, body)
}

const signup = (body) => {
  return api.post(`${subUrl}/signup`, body)
}

const auth = {
  login,
  signup,
}

export default auth
