import AccountBalanceIcon from '@mui/icons-material/AccountBalance'
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline'
import AssessmentIcon from '@mui/icons-material/Assessment'
import SettingsIcon from '@mui/icons-material/Settings'
import LocalAtmIcon from '@mui/icons-material/LocalAtm'
export const links = [
  { title: 'wallet', href: '/wallet', icon: <AccountBalanceIcon fontSize="large" /> },
  { title: 'target', href: '/budget', icon: <LocalAtmIcon fontSize="large" /> },
  { title: 'add record', href: '/add-record', icon: <AddCircleOutlineIcon fontSize="large" /> },
  { title: 'statistic', href: '/statistic', icon: <AssessmentIcon fontSize="large" /> },
  { title: 'setting', href: '/setting', icon: <SettingsIcon fontSize="large" /> },
]
