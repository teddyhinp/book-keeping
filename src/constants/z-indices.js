export const zIndices = {
  negative: -1,
  low: 1,
  mid: 10,
  high: 50,
  header: 100,
  sidebar: 200,
  modal: 300,
}
