export const palette = {
  primary: {
    200: '#FEEECF',
    400: '#F8B64C',
    600: '#FFCB05',
    800: '#A67700',
    main: '#FFCB05',
  },
  secondary: {
    200: '#F8F6FA',
    400: '#9561BE',
    600: '#6C22A6',
    800: '#4F137E',
    main: '#6C22A6',
  },
  neutral: {
    200: '#F9FAFB',
    400: '#9FA5AF',
    600: '#475467',
    800: '#101828',
    main: '#475467',
  },
  base: {
    white: '#FFFFFF',
    black: '#000000',
  },
}
