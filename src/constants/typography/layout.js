import { addPrefix } from '@/constants/typography/helper'

const PREFIX = 'LAYOUT'

const p = addPrefix.bind(null, PREFIX)

export const layoutTypographyEnum = {
  navBarTitle: p('navBarTitle'),
  date: p('date'),
  tabBarTitle: p('tabBarTitle'),
}

export const layoutTypography = (palette) => ({
  [layoutTypographyEnum.navBarTitle]: {
    fontSize: '16px',
    fontWeight: 700,
    color: palette.base.white,

    '&:hover': {
      color: palette.primary[400],
    },
  },
  [layoutTypographyEnum.date]: {
    fontSize: '16px',
    fontWeight: 700,
    color: palette.base.black,
    '&:hover': {
      color: palette.primary[400],
    },
  },

  [layoutTypographyEnum.tabBarTitle]: {
    fontSize: '12px',
    fontWeight: 400,
    color: palette.base.black,
    '&:hover': {
      color: palette.primary[400],
    },
  },
})
