export function addPrefix(prefix, key) {
  return `${prefix}-${key}`
}
export function up(value) {
  return `@media (min-width:${value}px)`
}
