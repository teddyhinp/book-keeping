import { addPrefix } from '@/constants/typography/helper'

const PREFIX = 'WALLET'

const p = addPrefix.bind(null, PREFIX)

export const walletTypographyEnum = {
  DialogTitle: p('DialogTitle'),
}

export const walletTypography = (palette) => ({
  [walletTypographyEnum.DialogTitle]: {
    fontSize: '16px',
    fontWeight: 700,
    color: palette.base.black,
  },
})
