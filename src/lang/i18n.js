import en from './en.json'
import tc from './tc.json'
import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'

i18n.use(initReactI18next).init({
  compatibilityJSON: 'v3',
  resources: {
    en,
    tc,
  },

  lng: 'en',
  fallbackLng: 'tc',
  interpolation: { escapeValue: false },
})

export default i18n
