import { createTheme } from '@mui/material/styles'
import { breakpoints, layoutTypography, palette } from '@/constants'
import { walletTypography } from './constants/typography/wallet'

export const theme = createTheme({
  palette,
  typography: (palette) => ({
    fontFamily: 'Inter, Arial, sans-serif',
    ...layoutTypography(palette),
    ...walletTypography(palette),
  }),
  breakpoints: {
    values: breakpoints,
  },
  shadows: [
    ...createTheme({}).shadows.map((shadow, index) => {
      switch (index) {
        case 1:
          return '0px 4px 4px rgba(0, 0, 0, 0.25)'
        default:
          return shadow
      }
    }),
  ],
  components: {
    MuiOutlinedInput: {
      styleOverrides: {
        root: {
          background: palette.neutral[200],
          fontSize: '14px',
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        root: {
          borderRadius: '8px',
        },
      },
    },
    MuiFormControl: {
      styleOverrides: {
        root: {
          '&.error': {
            '& .MuiOutlinedInput-notchedOutline': {
              borderWidth: '2px',
              borderColor: palette.primary[400],
            },
          },
        },
      },
    },
  },
})
