import { createSlice } from '@reduxjs/toolkit'

const slice = createSlice({
  name: 'wallet',
  initialState: {
    open: false,
  },
  reducers: {
    setOpen(state, action) {
      state.open = action.payload
    },
  },
})

export const { setOpen } = slice.actions
export default slice.reducer
