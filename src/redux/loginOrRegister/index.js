import { createSlice } from '@reduxjs/toolkit'

export const modeOption = Object.freeze({
  LOGIN: 'login',
  REGISTER: 'register',
})

const slice = createSlice({
  name: 'loginOrRegister',
  initialState: {
    currentMode: modeOption.LOGIN,
    isLogin: false,
  },
  reducers: {
    setCurrentMode(state, action) {
      state.currentMode = action.payload
    },
    setIsLogin(state, action) {
      state.isLogin = action.payload
    },
  },
})

export const { setCurrentMode, setIsLogin } = slice.actions
export default slice.reducer
