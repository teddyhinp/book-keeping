import { configureStore } from '@reduxjs/toolkit'
import AddWalletModalReducer from './wallet'
import LoginOrRegisterReducer from './loginOrRegister'

export const store = configureStore({
  reducer: {
    wallet: AddWalletModalReducer,
    loginOrRegister: LoginOrRegisterReducer,
  },
})
