import { CssBaseline, ThemeProvider } from '@mui/material'
import { theme } from '@/theme'
import Head from 'next/head'
import { Layout } from '@/components/Layout'
import { Provider } from 'react-redux'
import { store } from '@/redux/store'
import '../styles/globals.css'
import i18n from '../lang/i18n'
import { useEffect } from 'react'
import { useRouter } from 'next/router'
export default function App({ Component, pageProps }) {
  const router = useRouter()
  useEffect(() => {
    if (
      router.query.lang &&
      router.query.lang !== i18n.language &&
      i18n.languages.includes(router.query.lang)
    ) {
      i18n.changeLanguage(router.query.lang)
    }
  }, [router.query.lang])
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <Head>
          <meta name="viewport" content="width=414, user-scalable=no" />
        </Head>
        <CssBaseline />
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </ThemeProvider>
    </Provider>
  )
}
