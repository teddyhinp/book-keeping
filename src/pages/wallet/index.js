import { AddWalletBtn, AddWalletDialog, WalletList } from '@/components/Wallet'
import withAuth from '@/components/authWrap'
import { Stack } from '@mui/material'
import React from 'react'

function Wallet() {
  return (
    <Stack spacing="20px">
      <WalletList />
      <AddWalletBtn />
      <AddWalletDialog />
    </Stack>
  )
}

export default withAuth(Wallet)
