import React from 'react'

export default function PageNotFound() {
  return (
    <div className="flex justify-center items-center h-full font-bold text-3xl ">
      404 Page Not Found Error
    </div>
  )
}
